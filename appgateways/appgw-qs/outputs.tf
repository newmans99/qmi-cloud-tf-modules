output "appgw_hostname" {
  value = local.appgw_hostname
}

output "appgw_id" {
  value = var.is_enabled ? azurerm_public_ip.appgw-ip[0].id : null
}

output "appgw_public_ip" {
  value = var.is_enabled ? azurerm_public_ip.appgw-ip[0].ip_address : null
}

output "appgw_backend_address_pool_0_id" {
  value = var.is_enabled ? azurerm_application_gateway.qmi-app-gw[0].backend_address_pool[0].id : null
}
