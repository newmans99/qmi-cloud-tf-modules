
locals {
    appgw_hostname              = "${var.appgw_hostname}.${var.domain}"
    appgw_name                  = "qmi-appgw-qdc"

    backend_address_pool_qdc    = "${var.appgw_hostname}-qdc-bp"
    http_setting_name_8443      = "${var.appgw_hostname}-8443-setting"
    qdc_probe_name              = "${var.appgw_hostname}-qdcprobe"
    listener_name_8080          = "${var.appgw_hostname}-8080"
    listener_name_8443          = "${var.appgw_hostname}-8443"
    request_routing_rule_https  = "${var.appgw_hostname}-https-rule"
    request_routing_rule_8443   = "${var.appgw_hostname}-8443-rule"
    redirect_configuration_8080 = "${var.appgw_hostname}-8080redirect"
    request_routing_rule_8080   = "${var.appgw_hostname}-8080-rule"
    
}

data "azurerm_key_vault_secret" "cert-password" {
  name         = "star-qmi-qlikpoc-com-password"
  key_vault_id = var.key_vault_id
}


data "azurerm_key_vault_secret" "cert-data" {
  name         = "star-qmi-qlikpoc-com-data"
  key_vault_id = var.key_vault_id
}

resource "azurerm_public_ip" "appgw-ip" {
  # To enable or disable this resource
  count = var.is_enabled ? 1 : 0

  name                = "${local.appgw_name}-ip-${var.provision_id}"
  resource_group_name = var.app_gw_rg
  sku                 = "Standard"
  location            = var.location
  allocation_method   = "Static"
}

resource "azurerm_application_gateway" "qmi-app-gw" {
  # To enable or disable this resource
  count = var.is_enabled ? 1 : 0

  name                = "${local.appgw_name}-${var.provision_id}"
  resource_group_name = var.app_gw_rg
  location            = var.location

  sku {
    name     = "WAF_v2"
    tier     = "WAF_v2"
    capacity = 1
  }

  waf_configuration {
    enabled          = true
    firewall_mode    = "Prevention"
    rule_set_type    = "OWASP"
    rule_set_version = "3.0"

  }

  gateway_ip_configuration {
    name      = "app-gw-subnet"
    subnet_id = var.app_gw_subnet
  }

  frontend_port {
    name = "8443"
    port = 8443
  }

  frontend_port {
    name = "8080"
    port = 8080
  }

  frontend_ip_configuration {
    name                 = "app-gw-front-end-ip-config"
    public_ip_address_id = azurerm_public_ip.appgw-ip[count.index].id
  }

  ssl_certificate {
    name     = var.cert_name
    password = data.azurerm_key_vault_secret.cert-password.value
    data     = data.azurerm_key_vault_secret.cert-data.value
  }

  ssl_policy {
    policy_type = "Predefined"
    policy_name = "AppGwSslPolicy20170401"
  }

  backend_address_pool {
    name = local.backend_address_pool_qdc
  }

  backend_http_settings {
    name                  = local.http_setting_name_8443
    cookie_based_affinity = "Disabled"
    port                  = 8080
    protocol              = "Http"
    request_timeout       = 7600
    probe_name            = local.qdc_probe_name
  }

  probe {
    name                = local.qdc_probe_name
    protocol            = "Http"
    host                = local.appgw_hostname
    interval            = 30
    path                = "/qdc"
    timeout             = 30
    unhealthy_threshold = 3
  }

  http_listener {
    name                           = local.listener_name_8443
    ssl_certificate_name           = var.cert_name
    host_name                      = local.appgw_hostname
    frontend_ip_configuration_name = "app-gw-front-end-ip-config"
    frontend_port_name             = "8443"
    protocol                       = "Https"

  }

  http_listener {
    name                           = local.listener_name_8080
    host_name                      = local.appgw_hostname
    frontend_ip_configuration_name = "app-gw-front-end-ip-config"
    frontend_port_name             = "8080"
    protocol                       = "Http"
  }


  request_routing_rule {
    name                       = local.request_routing_rule_8443
    rule_type                  = "Basic"
    http_listener_name         = local.listener_name_8443
    backend_address_pool_name  = local.backend_address_pool_qdc
    backend_http_settings_name = local.http_setting_name_8443
  }


  redirect_configuration {
    name                 = local.redirect_configuration_8080
    target_listener_name = local.listener_name_8443
    redirect_type        = "Permanent"
    include_path         = true
    include_query_string = true
  }

  request_routing_rule {
    name                        = local.request_routing_rule_8080
    rule_type                   = "Basic"
    http_listener_name          = local.listener_name_8080
    redirect_configuration_name = local.redirect_configuration_8080

  }
}
