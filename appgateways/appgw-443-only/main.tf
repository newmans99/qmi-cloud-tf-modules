locals {
    appgw_hostname              = "${var.appgw_hostname}.${var.domain}"
    appgw_name                  = "qmi-appgw-443-only"

    # Qlik Sense
    backend_address_pool_qs     = "${var.appgw_hostname}-qs-bp"
    http_setting_name_443       = "${var.appgw_hostname}-http-setting"
    listener_name_http          = "${var.appgw_hostname}-http"
    listener_name_https         = "${var.appgw_hostname}-https"
    qs_probe_name               = "${var.appgw_hostname}-probe"
    request_routing_rule_https  = "${var.appgw_hostname}-https-rule"
    request_routing_rule_http   = "${var.appgw_hostname}-http-rule"
    redirect_configuration_80   = "${var.appgw_hostname}-80redirect"
}

data "azurerm_key_vault_secret" "cert-password" {
  name         = "star-qmi-qlikpoc-com-password"
  key_vault_id = var.key_vault_id
}


data "azurerm_key_vault_secret" "cert-data" {
  name         = "star-qmi-qlikpoc-com-data"
  key_vault_id = var.key_vault_id
}

resource "azurerm_public_ip" "appgw-ip" {
  # To enable or disable this resource
  count = var.is_enabled ? 1 : 0

  name                = "${local.appgw_name}-ip-${var.provision_id}"
  resource_group_name = var.app_gw_rg
  sku                 = "Standard"
  location            = var.location
  allocation_method   = "Static"
}

resource "azurerm_application_gateway" "qmi-app-gw" {
  # To enable or disable this resource
  count = var.is_enabled ? 1 : 0
  
  name                = "${local.appgw_name}-${var.provision_id}"
  resource_group_name = var.app_gw_rg
  location            = var.location

  sku {
    name     = "WAF_v2"
    tier     = "WAF_v2"
    capacity = 1
  }

  gateway_ip_configuration {
    name      = "app-gw-subnet"
    subnet_id = var.app_gw_subnet
  }

  waf_configuration {
    enabled               = true
    firewall_mode         = "Prevention"
    rule_set_type         = "OWASP"
    rule_set_version      = "3.0"
    file_upload_limit_mb  = "500"
  }

  # Qlik Sense fe ports
  frontend_port {
    name = "443"
    port = 443
  }
  frontend_port {
    name = "80"
    port = 80
  }

  frontend_ip_configuration {
    name                 = "app-gw-front-end-ip-config"
    public_ip_address_id = azurerm_public_ip.appgw-ip[count.index].id
  }

  ssl_certificate {
    name          = var.cert_name
    password      = data.azurerm_key_vault_secret.cert-password.value
    data          = data.azurerm_key_vault_secret.cert-data.value
  }

  ssl_policy {
    policy_type = "Predefined"
    policy_name = "AppGwSslPolicy20170401"
  }

  # Backend pool for QS, QIB and NP
  backend_address_pool {
    name = local.backend_address_pool_qs
  }


  # Qlik Sense
  backend_http_settings {
    name                  = local.http_setting_name_443
    cookie_based_affinity = "Disabled"
    port                  = 443
    protocol              = "Https"
    request_timeout       = 7600
    probe_name            = local.qs_probe_name
  }

  # QS Prob
  probe {
    name                = local.qs_probe_name
    protocol            = "Https"
    host                = local.appgw_hostname
    interval            = 30
    path                = "/"
    timeout             = 30
    unhealthy_threshold = 3
  }

  # Qlik Sense listener
  http_listener {
    name                           = local.listener_name_https
    host_name                      = local.appgw_hostname
    ssl_certificate_name           = var.cert_name
    frontend_ip_configuration_name = "app-gw-front-end-ip-config"
    frontend_port_name             = "443"
    protocol                       = "Https"

  }
  http_listener {
    name                           = local.listener_name_http
    host_name                      = local.appgw_hostname
    frontend_ip_configuration_name = "app-gw-front-end-ip-config"
    frontend_port_name             = "80"
    protocol                       = "Http"
  }

  # Qlik Sense routing rules (needs QS using qmi.qlik-poc.com certs)
  request_routing_rule {
    name                       = local.request_routing_rule_https
    rule_type                  = "Basic"
    http_listener_name         = local.listener_name_https
    backend_address_pool_name  = local.backend_address_pool_qs
    backend_http_settings_name = local.http_setting_name_443
  }
  

  # Redirect QS 80 to 443
  redirect_configuration {
    name                 = local.redirect_configuration_80
    target_listener_name = local.listener_name_https
    redirect_type        = "Permanent"
    include_path         = true
    include_query_string = true
  }
  request_routing_rule {
    name                        = local.request_routing_rule_http
    rule_type                   = "Basic"
    http_listener_name          = local.listener_name_http
    redirect_configuration_name = local.redirect_configuration_80
  }

}