This AppGw does:

- Open frontend ports: 
    - QS: https 443, http 80
    - QIB:  https 4435
    - NP: https 4993/4994
    - QDC: https 8443 (and http 8080 that redirects to 8443)

- For HTTPS to work for QS, QIB and NP need to use qmi.qlik-poc.com certificates.