resource "azurerm_monitor_diagnostic_setting" "example" {

    # To enable or disable this resource
    count = var.is_enabled ? 1 : 0

    name               = "${local.appgw_name}-${var.provision_id}-diagsettings"
    target_resource_id = azurerm_application_gateway.qmi-app-gw[0].id

    log_analytics_workspace_id = var.log_analytics_workspace_id

    log {
        category = "ApplicationGatewayAccessLog"
        enabled  = true

        retention_policy {
            enabled = true
            days = 7
        }
    }

    log {
        category = "ApplicationGatewayFirewallLog"
        enabled  = true

        retention_policy {
            enabled = true
            days = 7
        }
    }
}