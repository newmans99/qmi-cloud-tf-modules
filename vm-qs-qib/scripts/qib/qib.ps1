
Param(
    [string] $ExternalDomain = "qibtemp.qmi.qlik-poc.com",
    [int] $IsExternalAccess = 1
)

$computerName=$env:COMPUTERNAME

Function restartQIBServices
{
    Write-Log "Starting QIB Services on $computerName"

    Set-Service -Name "Qlik Insight Bot Deployment Engine Service" -StartupType Automatic
    Set-Service -Name "Qlik Insight Bot Duckling Service" -StartupType Automatic
    Set-Service -Name "Qlik Insight Bot Narrative Service" -StartupType Automatic
    Set-Service -Name "Qlik Insight Bot NLU Service"  -StartupType Automatic

    Start-Service -InputObject "Qlik Insight Bot Duckling Service"  -ErrorAction SilentlyContinue
    Start-Service -InputObject "Qlik Insight Bot Narrative Service"  -ErrorAction SilentlyContinue
    Start-Service -InputObject "Qlik Insight Bot NLU Service"  -ErrorAction SilentlyContinue
    Start-Service -InputObject "Qlik Insight Bot Deployment Engine Service"  -ErrorAction SilentlyContinue

    Write-Log "Restart QIB ISS services"
    Import-Module WebAdministration
    Stop-WebSite 'portal'
    Stop-WebSite 'engineservice'
    Stop-WebSite 'nlpservice'
    Stop-WebSite 'managementservice'
    Start-WebSite 'engineservice'
    Start-WebSite 'nlpservice'
    Start-WebSite 'managementservice'
    Start-WebSite 'portal'
}

#Remove-Item –path "c:\ProgramData\Qlik Insight Bot\Logs\*" -Force

Write-Log "Exporting new certificates"
Export-QlikCertificate -machineNames "localhost" -includeSecretsKey -exportFormat "Windows"
Export-QlikCertificate -machineNames "localhost" -includeSecretsKey -exportFormat "Pem"

Write-Log "Copy new certs to QIB location"
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\root.pem" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\server.pem" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\server_key.pem" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\client.pfx" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\client.pfx" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate\qmi-qs-1927.pfx" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\client.pfx" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate\$computerName.pfx" -Force

if ( $IsExternalAccess -eq 1 ) {
    ((Get-Content -path C:\provision\qib\NodeConfiguration.json -Raw) -replace '{{EXTERNAL_DOMAIN}}',$ExternalDomain) | Set-Content -Path C:\provision\qib\NodeConfiguration.json
} else {
    ((Get-Content -path C:\provision\qib\NodeConfiguration.json -Raw) -replace '{{EXTERNAL_DOMAIN}}',$computerName) | Set-Content -Path C:\provision\qib\NodeConfiguration.json
}

((Get-Content -path C:\provision\qib\NodeConfiguration.json -Raw) -replace '{{HOSTNAME}}',$computerName) | Set-Content -Path C:\provision\qib\NodeConfiguration.json
Copy-Item "C:\provision\qib\NodeConfiguration.json" -Destination "C:\ProgramData\Qlik Insight Bot\Qlik\NodeConfiguration.json" -Force

restartQIBServices

Stop-Service -InputObject "Qlik Insight Bot Deployment Engine Service"  -ErrorAction SilentlyContinue
Start-Service -InputObject "Qlik Insight Bot Deployment Engine Service"  -ErrorAction SilentlyContinue


