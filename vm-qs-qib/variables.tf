
variable "subnet_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-infra-vnet/providers/Microsoft.Network/virtualNetworks/QMI-Automation-Vnet/subnets/QMI-VM-Deployments"
}

variable "prefix" {
  description = "The Prefix used for all resources in this example"
  default = "QMI-QS-QIB"
}

variable "location" {
  default = "East US"
}

variable "image_reference" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.Compute/images/qliksense-qib-base-feb20-1"
}

variable "resource_group_name" {
}

variable "vm_type" {
  default = "Standard_D4s_v3"
}

variable "managed_disk_type" {
  default = "Premium_LRS"
}

variable "disk_size_gb" {
  default = "128"
}

variable "admin_username" {
  default = "qmi"
}

variable "user_id" {
}

variable "key_vault_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.KeyVault/vaults/qmisecrets"
}

variable "is_external_access" {
  type = number
  default = 1
}
