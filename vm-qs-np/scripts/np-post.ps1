Function restartNPServices
{
    write-log -Message "Starting NPrinting Services on $env:COMPUTERNAME"

    Set-Service -Name QlikNPrintingEngine -StartupType Automatic
    Set-Service -Name QlikNPrintingWebEngine -StartupType Automatic
    Set-Service -Name QlikNPrintingScheduler -StartupType Automatic
    Set-Service -Name QlikNPrintingMessagingService  -StartupType Automatic
    Set-Service -Name QlikNPrintingRepoService  -StartupType Automatic
    Set-Service -Name QlikNPrintingLicenseService  -StartupType Automatic

    Start-Service -InputObject QlikNPrintingRepoService  -ErrorAction SilentlyContinue
    Start-Service -InputObject QlikNPrintingMessagingService  -ErrorAction SilentlyContinue
    Start-Service -InputObject QlikNPrintingWebEngine  -ErrorAction SilentlyContinue
    Start-Service -InputObject QlikNPrintingEngine  -ErrorAction SilentlyContinue
    Start-Service -InputObject QlikNPrintingScheduler  -ErrorAction SilentlyContinue
    Start-Service -InputObject QlikNPrintingLicenseService -ErrorAction SilentlyContinue
}

Write-Log -Message "Exporting new certificates"
Export-QlikCertificate -machineNames "localhost" -includeSecretsKey -exportFormat "Windows"
Export-QlikCertificate -machineNames "localhost" -includeSecretsKey -exportFormat "Pem"


Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\client.pfx" -Destination "C:\Program Files\NPrintingServer\Settings\SenseCertificates" -Force

restartNPServices