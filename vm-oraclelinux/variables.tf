variable "subnet_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-infra-vnet/providers/Microsoft.Network/virtualNetworks/QMI-Automation-Vnet/subnets/QMI-VM-Deployments"
}

variable "prefix" {
  description = "The Prefix used for all resources in this example"
  default = "QMI-ORACLELINUX"
}

variable "location" {
  default = "East US"
}

variable "resource_group_name" {
}

variable "vm_type" {
  default = "Standard_DS3_v2"
}

variable "managed_disk_type" {
  default = "Premium_LRS"
}

variable "disk_size_gb" {
  default = "128"
}

variable "admin_username" {
  default = "qmi"
}

variable "user_id" {
}

# variable "subnet_id" {
#   default = "/subscriptions/1f3d4c1d-6509-4c52-8dee-c15fb83f2920/resourceGroups/lkn-rg/providers/Microsoft.Network/virtualNetworks/lkn-vn/subnets/default"
# }
