resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 3
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

locals {
  virtual_machine_name = "${var.prefix}-${random_id.randomMachineId.hex}"
  admin_username = var.admin_username
  admin_password = random_password.password.result
}

module "qmi-nic" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//qmi-nic"
  
  prefix = local.virtual_machine_name
  location = var.location
  subnet_id = var.subnet_id
  
  resource_group_name = var.resource_group_name
  user_id = var.user_id
}

resource "azurerm_virtual_machine" "vm" {
  name                  = local.virtual_machine_name
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [module.qmi-nic.id]
  vm_size               = var.vm_type

  delete_os_disk_on_termination = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${local.virtual_machine_name}-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = var.managed_disk_type
    disk_size_gb      = var.disk_size_gb
  }

  os_profile_linux_config {
      disable_password_authentication = false
  }

  os_profile {
    computer_name  = local.virtual_machine_name
    admin_username = local.admin_username
    admin_password = local.admin_password
  }

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
  }

  provisioner "file" {
    connection {
        type     = "ssh"
        host     = module.qmi-nic.private_ip_address
        user     = local.admin_username
        password = local.admin_password
        timeout  = "60s"
        #private_key = "${file("~/.ssh/id_rsa")}"   
    }
    source      = "${path.module}/scripts"
    destination = "~"
  }

  provisioner "remote-exec" {
    connection {
      type     = "ssh"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      timeout  = "60s"
      #private_key = "${file("~/.ssh/id_rsa")}"
    }

    inline = [
        "echo ${local.admin_password} | sudo -S chmod u+x /home/${local.admin_username}/scripts/*.sh",
        "sudo /home/${local.admin_username}/scripts/resizedisk.sh"
    ]
  }
}