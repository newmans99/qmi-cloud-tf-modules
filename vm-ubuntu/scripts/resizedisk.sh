
echo "Resizing main partition to max available disk"
(
echo p # List the partitions details
echo d # Delete  partition
echo 1 # Partition number
echo n # Add a new partition
echo 1 # Partition number
echo   # First sector (Accept default)
echo   # Last sector (Accept default: varies)
echo w # Write changes
) | sudo fdisk /dev/sdb

sudo partprobe

echo "Done resize!"
echo ""
df -h