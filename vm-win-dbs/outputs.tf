output "virtual_machine_id" {
  value = azurerm_virtual_machine.vm.id
}

output "virtual_machine_name" {
    value = azurerm_virtual_machine.vm.name
}

output "admin_username" {
  value = var.admin_username
}

output "admin_password" {
  value = random_password.password.result
}

output "nic_id" {
  value = module.qmi-nic.id
}

output "nic_private_ip_address" {
  value = module.qmi-nic.private_ip_address
}

output "nic_ip_configuration_name" {
  value = module.qmi-nic.ip_configuration_name
}

output "MySql-Database-Demo-User_Password" {
  value = "demo / Qlik1234 "
}

output "MySql-Database-Root-User_Password" {
  value = "root / Qlik1234"
}

output "MSSQL-Database-Demo-User_Password" {
  value = "demo / Qlik1234"
}

output "MSSQL-Database-Root-User_Password" {
  value = "sa / Qlik1234"
}

