#!/bin/bash

sudo mount -o remount,rw /

echo "Restart Postgres and QDC"
sudo service qdc_pg-11.2 restart 
sudo service qdc restart 


echo "Restart QlikCore"
sudo systemctl daemon-reload
sudo service containerd restart
sudo service docker stop
sudo service docker start
sudo systemctl restart qlikcore

sudo mount /usr/local/qdc/QVDs


