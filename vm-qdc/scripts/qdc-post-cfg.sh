
QSEHOST=$1
QSEIP=$2
QSEUSERNAME=$3
QSEPASSWORD=$4
APPGW_HOSTNAME=$5
IS_EXTERNAL=$6

echo "$QSEIP  $QSEHOST  $APPGW_HOSTNAME" >>/etc/hosts

echo "These are Qlik Sense connection parameters:" $QSEHOST $QSEIP $QSEUSERNAME $QSEPASSWORD $IS_EXTERNAL #$QSEPASSWORD
echo "AppGW Hostname: " $APPGW_HOSTNAME

#certs
echo "--- Setting up certs"

qlikpublishFolder=/usr/local/qdc/qlikpublish

#old - changed in december
#sudo -i -u qdc mkdir /usr/local/qdc/qliksense/certs
sudo -i -u qdc mkdir -p $qlikpublishFolder/certs
mkdir /tmp/certs


echo "--- Mount certs folder from Qlik Sense vm and copy certs to QDC Centos"
mount.cifs \\\\$QSEHOST\\certs /tmp/certs -o user=$QSEUSERNAME,pass=$QSEPASSWORD,uid=1001,gid=1001,vers=2.0
sudo -i -u qdc cp /tmp/certs/*.pem $qlikpublishFolder/certs/
sudo -i -u qdc cp -f /tmp/certs/qdc.jwt $qlikpublishFolder/qdc.jwt
echo "--- Umount certs folder from Qlik Sense vm"
sudo umount /tmp/certs


echo "--- These are the certs from Qlik Sense"
ls $qlikpublishFolder/certs/
echo "--- "
openssl x509 -inform PEM -outform DER -text -in $qlikpublishFolder/certs/root.pem -out $qlikpublishFolder/certs/root.der
# keytool -delete -alias qse-CA -keystore /etc/pki/ca-trust/extracted/java/cacerts -storepass changeit -noprompt
keytool -importcert -alias qse-CA  -keystore /etc/pki/ca-trust/extracted/java/cacerts -file $qlikpublishFolder/certs/root.der -storepass changeit -noprompt
cd $qlikpublishFolder


echo "--- Mount QVDs folder from Qlik Sense vm"
# //$QSEHOST/qvds /usr/local/qdc/QVDs    cifs    username=$QSEUSERNAME,password=$QSEPASSWORD,uid=1001,gid=1001,vers=2.0,noauto 0 0
mkdir -p /usr/local/qdc/QVDs
chown qdc:qdc /usr/local/qdc/QVDs
rm -f /etc/fstab
NewFile=/etc/fstab
(
cat <<EOF
//$QSEHOST/qvds /usr/local/qdc/QVDs    cifs    username=$QSEUSERNAME,password=$QSEPASSWORD,uid=1001,gid=1001,vers=2.0
EOF
) >> $NewFile
# mount -a
mount /usr/local/qdc/QVDs


search="userDirectory = 'EC'"
replace="userDirectory = '"$QSEHOST"'"
sed -i "s|$search|$replace|" ./createQlikApp.js
sed -i "s|$search|$replace|" ./updateExistingApp.js
sed -i "s|$search|$replace|" ./applist.js


if [ "$IS_EXTERNAL" = "nok" ]; then
  APPGW_HOSTNAME=$QSEHOST
  echo "--- This Only PRIVATE deployment, so APPGW_HOSTNAME=$APPGW_HOSTNAME"
fi

jwtToken=`cat $qlikpublishFolder/qdc.jwt`
TOMCAT_HOME="/usr/local/qdc/apache-tomcat-7.0.99"
echo "------- Qlik Sense JWT Token ----"
echo $jwtToken
echo "---------------------------------"
propFile=$TOMCAT_HOME/conf/core_env.properties
(
cat <<EOF

### START -- PUBLISH TO QLIK SETUP
is.publish.to.qlik.enabled=true
qlik.sense.active.directory.name=$QSEHOST
qlik.sense.url=https://$APPGW_HOSTNAME/sense/app/<podium-gen-app-id>
qlik.sense.jwt=$jwtToken
is.publish.to.tableau.enabled=true
podium.qlik.appcreation.qsockclient.script=/usr/local/qdc/qlikpublish/createQlikApp.js
podium.qlik.appUpdate.qsockclient.script=/usr/local/qdc/qlikpublish/updateExistingApp.js
podium.qlik.dataconnection.name=podium_dist
### END -- PUBLISH TO QLIK SETUP

EOF
) >> $propFile

systemctl restart qdc


echo "Installing Crontab to rescue disk after reboot"
chmod a+x /home/qmi/scripts/rescue.sh
(sudo crontab -l ; echo "@reboot /home/qmi/scripts/rescue.sh") | sudo crontab -
echo "Done!"
