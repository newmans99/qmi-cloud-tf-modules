
echo "Resizing main partition to max available disk"
(
echo u # Change the units to sectors
echo p # List the partitions details
echo d # Delete  partition
echo 2 # Partition number
echo n # Add a new partition
echo p # Primary partition
echo 2 # Partition number
echo   # First sector (Accept default)
echo   # Last sector (Accept default: varies)
echo w # Write changes
) | sudo fdisk /dev/sda

sudo partprobe
sudo xfs_growfs -d /

echo "Done resize!"
echo ""
df -h