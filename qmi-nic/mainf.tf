
/*resource "azurerm_network_security_group" "example" {
  name                = "${var.prefix}-${var.provision_id}-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
  }

  security_rule {
    name                       = "RDP"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "WinRM"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5985"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTP"
    priority                   = 120
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTPS"
    priority                   = 140
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}*/

/*resource "azurerm_virtual_network" "example" {
  name                = "${var.prefix}-network"
  address_space       = ["10.0.0.0/16"]
  location            = "${azurerm_resource_group.example.location}"
  resource_group_name = "${azurerm_resource_group.example.name}"
}*/

/*resource "azurerm_subnet" "example" {
  #name                 = "internal"
  name                = "${var.prefix}-subnet"
  resource_group_name  = "${azurerm_resource_group.example.name}"
  virtual_network_name = "${azurerm_virtual_network.example.name}"
  address_prefix       = "10.0.2.0/24"
}*/

/*resource "azurerm_public_ip" "example" {
  name                = "${var.prefix}-publicip"
  resource_group_name = "${azurerm_resource_group.example.name}"
  location            = "${azurerm_resource_group.example.location}"
  allocation_method   = "Static"
}*/

resource "azurerm_network_interface" "nic" {
  name                = "${var.prefix}-nic"
  location            = var.location
  resource_group_name = var.resource_group_name
  //network_security_group_id       = azurerm_network_security_group.example.id

  ip_configuration {
    name                          = "${var.prefix}-ip-cfg"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Dynamic"
    //public_ip_address_id          = "${azurerm_public_ip.example.id}"
  }

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
  }
}

/*resource "azurerm_subnet_network_security_group_association" "example" {
  subnet_id                 = "/subscriptions/a2716055-c172-463a-88f6-46c34305a3e9/resourceGroups/IT-Infra-Mgmt-DevTest/providers/Microsoft.Network/virtualNetworks/IT-Infra-Mgmt-DevTest-Vnet/subnets/DevTest-Secure"
  network_security_group_id = azurerm_network_security_group.example.id
}*/