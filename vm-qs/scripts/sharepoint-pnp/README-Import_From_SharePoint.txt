Hi there!

Do you have a set of extensions and apps that you always use for your Qlik Sense demos?

You can import these Apps and Extensions right away into Qlik Sense using the script 'Import_From_SharePoint.ps1' you'll find on the Desktop.

HOW TO
======

1. Navigate to Qlik SharePoint online: https://qliktechnologies365-my.sharepoint.com
 
Here you need a folder with this structure:

    Your_Folder 
        - Extensions    
            - extension1.zip
            - extension2.zip
            ...
        - Apps
            - app1.qvf
            - app2.qvf
            - SubFolder1
                - app3.qvf
            - SubFolder2
                - app4.qvf
            ...

2. Navigate into that folder in your browser.Then copy the entire Url from the browser. It should look something similar to this:

    https://qliktechnologies365-my.sharepoint.com/personal/<YOUR_TRIGRAM>_qlik_com1/_layouts/15/onedrive.aspx?id=<SOME STUFF HERE>

    Copy the whole URL!!

3. RDP with user 'qmi' into the VM that runs Qlik Sense (important! with user 'qmi').

4. On the Desktop, right click on 'Import_From_SharePoint.ps1' and select 'Run with powershell'.

5. You'll be asked to enter the 'SharePoint Url'. Paste the url from 2.)

6. You'll be asked 'SharePoint login'. Enter your username as trigram@qlik.com and password.

7. That's it! 

It will download the content and import the Extensions and Apps (and create streams out of 'SubFolderX') in Qlik Sense.

Enjoy!!!
