Function DownloadFilesFromOneDrive() {
    Process{

        Add-Type -AssemblyName System.Web

        try {
 
            # --- Ask for URL and parse it
            $uri = Read-Host -Prompt 'SharePoint Url'
            $decodedURL = [System.Web.HttpUtility]::UrlDecode($uri)
            $decoded = [uri] $decodedURL
            $url = "$($decoded.Scheme)://$($decoded.Host)$($decoded.AbsolutePath)"

            # ---- Get 'id' param from URL
            $ParsedQueryString = [System.Web.HttpUtility]::ParseQueryString($decoded.Query)
            $i = 0
            $folderSiteRelativeUrl = ""
            foreach($QueryStringObject in $ParsedQueryString){
                if ( $QueryStringObject -eq "id" ) {
                    $folderSiteRelativeUrl = $ParsedQueryString[$i]
                    break
                }
                $i++
            }
            $ParamId = $folderSiteRelativeUrl
            $decoded.Segments | ForEach-Object {
                [regex]$pattern = $_
                $ParamId = $pattern.replace($ParamId, "", 1) 
            }
            Write-Host "FolderSiteRelativeUrl: $ParamId"
            # ----

            # --- Get OneDrive credentials and Connect
            $credentials = Get-Credential -Message "SharePoint login:" -UserName "<trigram>@qlik.com"
            Connect-PnPOnline -Url $url -Credentials $credentials 
            # --- 

            # Create folders and download files from OneDrive
            New-Item -Path "C:\Temp" -Name "qs-import" -ItemType "directory" -Force | Out-Null
            $files = Get-PnPFolderItem -FolderSiteRelativeUrl $ParamId -ItemType File -Recursive | select ServerRelativeUrl,Name,TypedObject
            $files | ForEach-Object {
                # Create folder
                $replacedPath = $($_.ServerRelativeUrl).Replace($folderSiteRelativeUrl,"")
                $folderName = Split-Path -Path $replacedPath        
                New-Item -Path "C:\Temp\qs-import" -Name $folderName -ItemType "directory" -Force | Out-Null
                
                # Download file into folder
                Write-Host "Downloading file: '$($_.ServerRelativeUrl)'"     
                Get-PnPFile -Url $($_.ServerRelativeUrl) -Path "C:\Temp\qs-import$folderName" -AsFile -Force          
            }

        } catch {
            Write-Host $_.Exception.Message -ForegroundColor Red
        }
    }
}

Function restartQse {
    Write-Host "Checking Engine Service has started..."
    $qse = Get-Service QlikSenseEngineService
    Write-Host "The engine is currently $($qse.Status)"
    if ($qse.Status -eq "Stopped") {
        Write-Host "Starting Qlik Sense Engine and waiting 60 seconds"; 
        Start-Service QlikSenseEngineService; 
        Restart-Service QlikSenseServiceDispatcher; 
        start-sleep -s 60
    }
    Write-Host "The engine is currently $($qse.Status)"
}


# START HERE
DownloadFilesFromOneDrive

Import-Module Qlik-Cli


### Connect to the Qlik Sense Repository Service with Qlik-Cli
do {
    Write-Host "--- Connecting to Qlik Sense Repository"; start-sleep 5
} 
While( (Connect-Qlik $($env:COMPUTERNAME) -TrustAllCerts -UseDefaultCredentials -ErrorAction SilentlyContinue).length -eq 0 )


### Import scenario extensions
Write-Host "Importing extensions from C:\Temp\qs-import\Extensions"
if ( Test-Path "C:\Temp\qs-import\Extensions" ) {
    gci C:\\Temp\\qs-import\\Extensions\\*.zip | foreach {
        try {
            Write-Host "Importing $_";
            Import-QlikExtension -ExtensionPath $_.FullName | Out-Null
        } catch {
            Write-Host $_.Exception.Message -ForegroundColor Red
        }
    }
}

### Import scenario applications
Write-Host "Connecting as user Qlik to QRS"
try {
    $cert = "CN=$env:COMPUTERNAME-ca"
    gci cert:\CurrentUser\My | where {$_.issuer -eq $cert} | Connect-Qlik -username "$env:COMPUTERNAME\qlik" | Out-Null
} catch {
    Write-Host $_.Exception.Message -ForegroundColor Red
}

Write-Host "Importing applications from C:\Temp\qs-import\Apps"
If (Test-Path "C:\Temp\qs-import\Apps\") {
    gci C:\\Temp\\qs-import\\Apps\\*.qvf | foreach {
        try {
            Write-Host "Importing $_";
            Import-QlikApp -name $_.BaseName -file $_.FullName -upload | Out-Null
        } catch {
            Write-Host $_.Exception.Message -ForegroundColor Red
        }
    }
}

restartQse

$apps = gci C:\Temp\qs-import\Apps\ -Directory
foreach ($subDirectory in $apps) {
    # $encodeDirectory = [System.Web.HttpUtility]::UrlEncode($subDirectory);
    $streams = $(Get-QlikStream -filter "name eq '$($subDirectory)'").name
    if ( $streams -ne $subDirectory ) {
        Write-Host "Creating $subDirectory stream"
        New-QlikStream $subDirectory | Out-Null;
        $streamId = $(Get-QlikStream -filter "name eq '$($subDirectory)'").id
        $systemRuleJson = (@{
            name = "Grant everyone access to $subDirectory";
            category = "Security";
            rule = '((user.name like "*"))';
            type = "Custom";
            resourceFilter = "Stream_$streamId";
            actions = 34;
            ruleContext = "QlikSenseOnly";
            disabled = $false;
            comment = "Stream access";} | ConvertTo-Json -Compress -Depth 10)
            Write-Host "Creating $subDirectory System Rule"
        Invoke-QlikPost "/qrs/systemrule" $systemRuleJson | Out-Null
    }
    
    $files = gci C:\Temp\qs-import\Apps\$subDirectory\*.qvf -File

    foreach ($file in $files) {
        $streamId = $(Get-QlikStream -filter "name eq '$($subDirectory)'").id
        # $encode = [System.Web.HttpUtility]::UrlEncode($file.BaseName)
        Write-Host "Importing $($file)";
        Import-QlikApp -name $file.BaseName -file $file.FullName -upload  | Out-Null;
        Write-Host "Publishing $($file.BaseName) to $($subDirectory)";
        Publish-QlikApp -id $(Get-QlikApp -filter "name eq '$($file.BaseName)'").id -stream $streamId -name $file.BaseName  | Out-Null
    }
}
 
