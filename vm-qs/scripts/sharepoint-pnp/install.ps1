[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$msiDownload = "https://github.com/pnp/PnP-PowerShell/releases/download/3.22.2006.0/SharePointPnPPowerShellOnline.msi"
Invoke-WebRequest -Uri $msiDownload -OutFile "C:\Temp\SharePointPnPPowerShellOnline.msi"
Start-Process "msiexec.exe" -Wait -ArgumentList '/I C:\Temp\SharePointPnPPowerShellOnline.msi /quiet'


Copy-Item -Path C:\provision\sharepoint-pnp\Import_From_SharePoint.ps1 -Destination C:\Users\Public\Desktop\ -Force
Copy-Item -Path C:\provision\sharepoint-pnp\README-Import_From_SharePoint.txt -Destination C:\Users\Public\Desktop\ -Force

