Write-Log -Message "Generating JWT for QDC"

Expand-Archive -LiteralPath "$PSScriptRoot\jwt-generator.zip" -DestinationPath c:\provision\
$ENV:PATH += ";C:\Program Files\Qlik\Sense\ServiceDispatcher\Node"
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\.Local Certificates\server*.pem" c:\provision\jwt-generator\.
node c:\provision\jwt-generator\index.js

Copy-Item c:\provision\jwt-generator\qdc.jwt "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\.Local Certificates\."


