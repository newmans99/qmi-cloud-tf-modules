# Shared variables here
Write-Log -Message "Setting shared variables for scenario"

# Dec 2019
#$PROXY_ARTIFACTS = "https://d7ipctdjxxii4.cloudfront.net/others/qdc/qdc-dec-2019/qdc_proxy_artifacts.zip"
# April 2020
$PROXY_ARTIFACTS = "https://da3hntz84uekx.cloudfront.net/QlikDataCatalyst/4.5/0/_MSI/qdc_proxy_artifacts.zip"