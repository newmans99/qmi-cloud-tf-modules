use qlikalerting;
show collections;

db.users.find().forEach(function (itemUser){
    itemUser.domain = getHostName();
    itemUser.domainUser = getHostName() + "/" + itemUser.name;
    itemUser.identity.qlik.userDirectory = getHostName();
    db.users.save(itemUser);
});


db.users.find();