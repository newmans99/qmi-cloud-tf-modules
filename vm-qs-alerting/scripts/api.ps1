
Param(
    [string] $IsExternal,
    [string] $Url
)

Write-Log -Message "Restarting hMailServer"
Restart-Service hMailServer -Force

Write-Log -Message "Modifying Qlik Alerting configuration for this hostname"

Expand-Archive -LiteralPath "$PSScriptRoot\apinode.zip" -DestinationPath c:\provision\
$ENV:PATH += ";C:\Program Files\Qlik\Sense\ServiceDispatcher\Node"
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\.Local Certificates\client*.pem" c:\provision\apinode\certs\.
if ( $IsExternal -eq "ok" ) {
    node c:\provision\apinode\index.js $Url
} else {
    node c:\provision\apinode\index.js
}


Write-Log "Recovering Qlik Alerting users for new hotname"
Start-Process powershell.exe -Wait -ArgumentList "Start-Process cmd.exe -Verb runAs -ArgumentList '/c C:\provision\reset-mongo.bat'"

Start-Sleep -s 10